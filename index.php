<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';

$config['displayErrorDetails'] = true;
$app = new \Slim\App(['settings' => $config]);

// Configura la rutas.
configHtmlRoute($app, '/', './templates/index.html');
configHtmlRoute($app, '/pagina/1', './templates/pagina1.html');
configHtmlRoute($app, '/pagina/2', './templates/pagina2.html');

// Ejecuta la aplicación.
$app->run();

/** 
 * configHtmlRoute
 * Configura la ruta dada en la aplicación como GET y devuelve el template dado como contenido.
 * @param {Slim} $app Aplicación.
 * @param {string} $route Ruta de la aplicación.
 * @param {string} $templatePath Ruta del template html.
 * @return {void}
 */
function configHtmlRoute($app, $route = '', $templatePath = '')  {
	$app->get($route, function($request, $response) use ($templatePath) {
		$fh = fopen($templatePath, 'rb');
		$stream = new Slim\Http\Stream($fh);
		return $response
				->withBody($stream)
				->withHeader('Content-Type', 'text/html; charset=utf-8');
	})->setOutputBuffering(false);
}