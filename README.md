# README

Requiere Composer https://getcomposer.org/ para instalar las dependencias.

Instalar composer

Ejecutar en una ventana de terminal en la carpeta del proyecto para que se instalen las dependencias de Slim. 

```
#!bash
php composer.phar install
```